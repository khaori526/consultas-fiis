
	function mostrarModalRespuesta(){
		$('#modalRespuesta').modal('show');
		$('#descripcionRespuesta').val('');
	}
	
	function mostrarModalPregunta(){
		$('#modalPregunta').modal('show');
		$('#descripcionPregunta').val('');
	}

	function mostrarModalRespuestaModificar(descripcion, idRespuesta){
		$('#modalRespuestaModificar').modal('show');
		$('#descripcionRespuestaModificar').val(descripcion);
		$('#idRespuestaModificar').val(idRespuesta);
	}

	function mostrarModalPreguntaModificar(descripcion, idPregunta){
		$('#modalPreguntaModificar').modal('show');
		$('#descripcionPreguntaModificar').val(descripcion);
		$('#idPreguntaModificar').val(idPregunta);
	}
	
	function mostrarModalUsuario(){
		$('#modalUsuario').modal('show');
		$('#nombresUsuario').val('');
		$('#apellidoPaternoUsuario').val('');
		$('#apellidoMaternoUsuario').val('');
		$('#correoUsuario').val('');
		$('#usernameUsuario').val('');
		$('#passwordUsuario').val('');
		$("#escuelaUsuario").prop("selectedIndex", 0).val();
		$("#rolUsuario").prop("selectedIndex", 0).val();
		$("#activoUsuario").attr('checked', true); 
	}
	
	function mostrarModalUsuarioModificar(idUsuario, nombres, apellidoPaterno, apellidoMaterno, username, password, correo, idRol, idEscuela, activo){
		$('#modalUsuarioModificar').modal('show');
		$('#idUsuarioModificar').val(idUsuario);
		$('#nombresUsuarioModificar').val(nombres);
		$('#apellidoPaternoUsuarioModificar').val(apellidoPaterno);
		$('#apellidoMaternoUsuarioModificar').val(apellidoMaterno);
		$('#correoUsuarioModificar').val(correo);
		$('#usernameUsuarioModificar').val(username);
		$('#passwordUsuarioModificar').val(password);
		$("#escuelaUsuarioModificar").val(idEscuela);
		$("#rolUsuarioModificar").val(idRol);
		$("#activoUsuarioModificar").attr('checked', activo); 
	}