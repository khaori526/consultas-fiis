package com.unfv.consultasfiis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unfv.consultasfiis.entity.Pregunta;


@Repository
public interface IPreguntaRepository extends JpaRepository<Pregunta, Integer>{
	
	public Page<Pregunta> findAllByOrderByFechaDesc(Pageable pageable);
	public Page<Pregunta> findAllByUsuario_Escuela_IdOrderByFechaDesc(Integer idEscuela, Pageable pageable);

}
