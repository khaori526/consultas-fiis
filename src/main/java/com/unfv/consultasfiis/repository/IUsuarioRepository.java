package com.unfv.consultasfiis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.unfv.consultasfiis.entity.Usuario;

public interface IUsuarioRepository extends JpaRepository<Usuario, Integer>{

//	@Query(value = "SELECT u FROM User u JOIN u.authorities a WHERE a.nombre = :rol")
//	public Page<User> findAll(@Param("rol") String rol, Pageable pageable);
//	
//	public Page<User> findByAuthorities_Name(AuthorityType authority, Pageable pageable);
	
	public List<Usuario> findByUsernameAndPassword(String username, String password);
	public Page<Usuario> findAllByOrderByApellidoPaternoAsc(Pageable pageable);
	
}
