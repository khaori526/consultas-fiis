package com.unfv.consultasfiis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unfv.consultasfiis.entity.Rol;


@Repository
public interface IRolRepository extends JpaRepository<Rol, Integer>{
	

}
