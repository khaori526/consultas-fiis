package com.unfv.consultasfiis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unfv.consultasfiis.entity.Escuela;


@Repository
public interface IEscuelaRepository extends JpaRepository<Escuela, Integer>{
	

}
