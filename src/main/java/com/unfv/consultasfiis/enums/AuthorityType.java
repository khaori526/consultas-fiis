package com.unfv.consultasfiis.enums;

public enum AuthorityType {

	ROLE_ADMINISTRADOR,
	ROLE_ADMINISTRATIVO,
	ROLE_ALUMNO
    
}
