package com.unfv.consultasfiis.service;


import java.util.List;


import com.unfv.consultasfiis.entity.Rol;

public interface IRolService {
	
	public List<Rol> findAll();
	
}
