package com.unfv.consultasfiis.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.unfv.consultasfiis.entity.Pregunta;

public interface IPreguntaService extends CRUDService<Pregunta>{
	
	public Page<Pregunta> findAllByOrderByFechaDesc(Pageable pageable);
	public Page<Pregunta> findByUsuario_Escuela_IdByOrderByFechaDesc(Integer idEscuela, Pageable pageable);
	
}
