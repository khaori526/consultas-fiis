package com.unfv.consultasfiis.service;


import java.util.List;

import com.unfv.consultasfiis.entity.Escuela;

public interface IEscuelaService {
	
	public List<Escuela> findAll();
	
}
