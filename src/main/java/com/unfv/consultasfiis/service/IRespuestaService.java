package com.unfv.consultasfiis.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.unfv.consultasfiis.entity.Respuesta;

public interface IRespuestaService extends CRUDService<Respuesta>{
	
	public Page<Respuesta> findAllByOrderByFechaDesc(Pageable pageable);
	
}
