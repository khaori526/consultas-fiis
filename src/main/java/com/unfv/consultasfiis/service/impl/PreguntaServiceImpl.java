package com.unfv.consultasfiis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.unfv.consultasfiis.entity.Pregunta;
import com.unfv.consultasfiis.repository.IPreguntaRepository;
import com.unfv.consultasfiis.service.IPreguntaService;

@Service
public class PreguntaServiceImpl implements IPreguntaService{

	@Autowired
	private IPreguntaRepository preguntaRepository;
	
	@Override
	public List<?> listAll() {
		return preguntaRepository.findAll();
	}

	@Override
	public Pregunta getById(Integer id) {
		return preguntaRepository.findById(id).orElse(null);
	}

	@Override
	public Pregunta saveOrUpdate(Pregunta domainObject) {
		return preguntaRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		preguntaRepository.deleteById(id);
	}

	@Override
	public Page<Pregunta> findAllByOrderByFechaDesc(Pageable pageable) {
		return preguntaRepository.findAllByOrderByFechaDesc(pageable);
	}

	@Override
	public Page<Pregunta> findByUsuario_Escuela_IdByOrderByFechaDesc(Integer idEscuela, Pageable pageable) {
		return preguntaRepository.findAllByUsuario_Escuela_IdOrderByFechaDesc(idEscuela, pageable);
	}

}
