package com.unfv.consultasfiis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unfv.consultasfiis.entity.Escuela;
import com.unfv.consultasfiis.repository.IEscuelaRepository;
import com.unfv.consultasfiis.service.IEscuelaService;

@Service
public class EscuelaServiceImpl implements IEscuelaService{

	@Autowired
	private IEscuelaRepository escuelaRepository;
	
	@Override
	public List<Escuela> findAll() {
		return escuelaRepository.findAll();
	}

}
