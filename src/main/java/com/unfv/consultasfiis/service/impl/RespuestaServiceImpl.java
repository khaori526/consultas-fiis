package com.unfv.consultasfiis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.unfv.consultasfiis.entity.Respuesta;
import com.unfv.consultasfiis.repository.IRespuestaRepository;
import com.unfv.consultasfiis.service.IRespuestaService;

@Service
public class RespuestaServiceImpl implements IRespuestaService{

	@Autowired
	private IRespuestaRepository iRespuestaRepository;
	
	@Override
	public List<?> listAll() {
		return iRespuestaRepository.findAll();
	}

	@Override
	public Respuesta getById(Integer id) {
		return iRespuestaRepository.findById(id).orElse(null);
	}

	@Override
	public Respuesta saveOrUpdate(Respuesta domainObject) {
		return iRespuestaRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		iRespuestaRepository.deleteById(id);
	}

	@Override
	public Page<Respuesta> findAllByOrderByFechaDesc(Pageable pageable) {
		return iRespuestaRepository.findAllByOrderByFechaDesc(pageable);
	}

}
