package com.unfv.consultasfiis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unfv.consultasfiis.entity.Rol;
import com.unfv.consultasfiis.repository.IRolRepository;
import com.unfv.consultasfiis.service.IRolService;

@Service
public class RolServiceImpl implements IRolService{

	@Autowired
	private IRolRepository rolRepository;
	
	@Override
	public List<Rol> findAll() {
		return rolRepository.findAll();
	}


}
