package com.unfv.consultasfiis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.unfv.consultasfiis.entity.Usuario;
import com.unfv.consultasfiis.repository.IUsuarioRepository;
import com.unfv.consultasfiis.service.IUsuarioService;
import com.unfv.consultasfiis.util.NcCrypt;

@Service
public class UsuarioServiceImpl implements IUsuarioService{
	
	@Autowired
	private IUsuarioRepository iUsuarioRepository;

	@Override
	public List<Usuario> findAll() {
		return iUsuarioRepository.findAll();
	}

	@Override
	public Page<Usuario> findAllByOrderByApellidoPaternoAsc(Pageable pageable) {
		return iUsuarioRepository.findAllByOrderByApellidoPaternoAsc(pageable);
	}

	@Override
	public Usuario saveOrUpdate(Usuario usuario) {
		usuario.setPassword(NcCrypt.encriptarPassword(usuario.getPassword()));
		return iUsuarioRepository.save(usuario);
	}

	@Override
	public void delete(Integer id) {
		iUsuarioRepository.deleteById(id);
	}

}
