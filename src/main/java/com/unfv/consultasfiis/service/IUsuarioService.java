package com.unfv.consultasfiis.service;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.unfv.consultasfiis.entity.Usuario;

public interface IUsuarioService {
	
//	public Page<Usuario> findAll(AuthorityType authority, Pageable pageable);
	
	public List<Usuario> findAll();
	public Page<Usuario> findAllByOrderByApellidoPaternoAsc(Pageable pageable);
	public Usuario saveOrUpdate(Usuario usuario);
	void delete(Integer id);
	
}
