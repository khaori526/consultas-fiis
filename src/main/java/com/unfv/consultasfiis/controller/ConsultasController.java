package com.unfv.consultasfiis.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.SessionFlashMapManager;

import com.unfv.consultasfiis.entity.Pregunta;
import com.unfv.consultasfiis.entity.Respuesta;
import com.unfv.consultasfiis.entity.Usuario;
import com.unfv.consultasfiis.service.IPreguntaService;
import com.unfv.consultasfiis.service.IRespuestaService;
import com.unfv.consultasfiis.util.PageRender;

@Controller("/consultas")
public class ConsultasController {
	
	private Logger logger = LoggerFactory.getLogger(ConsultasController.class);
	
	@Autowired
	private IPreguntaService iPreguntaService;
	
	@Autowired
	private IRespuestaService iRespuestaService;
	
	@GetMapping(value = {"/listar", "/"})
	public String listar(@RequestParam(name = "page", defaultValue = "0") int page, Model model,
			Authentication authentication) {

		if(authentication != null) {
			logger.info("Hola usuario autenticado, tu username es: ".concat(((Usuario)authentication.getPrincipal()).getUsername()));
		}
		
		Pageable pageRequest = PageRequest.of(page, 10);

		Page<Pregunta> listaPreguntas = iPreguntaService.findAllByOrderByFechaDesc(pageRequest);

		PageRender<Pregunta> pageRender = new PageRender<Pregunta>("/listar", listaPreguntas);
		model.addAttribute("listaPreguntas", listaPreguntas);
		model.addAttribute("page", pageRender);
		return "listar";
	}
	
	@GetMapping("/respuestas/{idPregunta}")
	public String update(@PathVariable("idPregunta") String idPregunta, Model model, Authentication authentication) {
		Usuario usuario = null;
		if(authentication != null) {
			usuario = (Usuario)authentication.getPrincipal();
		}
		Pregunta pregunta = iPreguntaService.getById(Integer.parseInt(idPregunta));
		model.addAttribute("pregunta", pregunta);
		model.addAttribute("idUser", usuario.getId());
		return "verpregunta";
	}
	
	@PostMapping("/respuestas/{idPregunta}")
	public String save(@ModelAttribute("respuestaEnviar") Respuesta respuesta
			, @PathVariable("idPregunta") Integer idPregunta, Authentication authentication, HttpServletRequest request, HttpServletResponse response) 
	{
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		Usuario usuario = null;
		if(authentication != null) {
			usuario = (Usuario)authentication.getPrincipal();
		}
		Pregunta pregunta = new Pregunta();
		pregunta.setId(idPregunta);
		respuesta.setPregunta(pregunta);
		respuesta.setUsuario(usuario);
		try {
			Respuesta insert = iRespuestaService.saveOrUpdate(respuesta);	
			if(insert!=null) {
				System.out.println("Se inserto la respuesta correctamente");
				flashMap.put("success", "Se insertó la respuesta correctamente!");
			}
			else {
				System.out.println("Error al insertar respuesta");
				flashMap.put("error", "Error al insertar la respuesta!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al insertar la respuesta!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/respuestas/" + idPregunta;
	}
	
	@PostMapping("/respuestas/editar/{idPregunta}")
	public String update(Respuesta respuesta
			, @PathVariable("idPregunta") Integer idPregunta, HttpServletRequest request, HttpServletResponse response) 
	{
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		try {
			Respuesta update = iRespuestaService.saveOrUpdate(respuesta);	
			if(update!=null) {
				System.out.println("Se modifico la respuesta correctamente");
				flashMap.put("success", "Se modificó la respuesta correctamente!");
			}
			else {
				System.out.println("Error al modificar la respuesta");
				flashMap.put("error", "Error al modificar la respuesta!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al modificar la respuesta!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/respuestas/" + idPregunta;
	}
	
	@GetMapping("/respuestas/eliminar/{idRespuesta}/{idPregunta}")
	public String delete(@PathVariable("idRespuesta") String idRespuesta, @PathVariable("idPregunta") String idPregunta,
			HttpServletRequest request, HttpServletResponse response) {
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		try {
			iRespuestaService.delete(Integer.parseInt(idRespuesta));
			flashMap.put("success", "Se eliminó la respuesta correctamente!");
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al eliminar la respuesta!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/respuestas/" + idPregunta;
	}
	
	@PostMapping("/pregunta/editar")
	public String update(Pregunta pregunta, HttpServletRequest request, HttpServletResponse response) 
	{
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		try {
			Pregunta update = iPreguntaService.saveOrUpdate(pregunta);	
			
			if(update!=null) {
				System.out.println("Se modifico la pregunta correctamente");
				flashMap.put("success", "Se modificó la pregunta correctamente!");
			}
			else {
				System.out.println("Error al modificar la pregunta");
				flashMap.put("error", "Error al modificar la pregunta!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al modificar la pregunta!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/respuestas/" + pregunta.getId();
	}

	@PostMapping("/pregunta/create")
	public String save(Pregunta pregunta, Authentication authentication, HttpServletRequest request, HttpServletResponse response) 
	{
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		Usuario usuario = null;
		if(authentication != null) {
			usuario = (Usuario)authentication.getPrincipal();
		}
		pregunta.setUsuario(usuario);
		try {
			Pregunta insert = iPreguntaService.saveOrUpdate(pregunta);	
			if(insert!=null) {
				System.out.println("Se inserto la pregunta correctamente");
				flashMap.put("success", "Se insertó la pregunta correctamente!");
			}
			else {
				System.out.println("Error al insertar la pregunta");
				flashMap.put("error", "Error al insertar la pregunta!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al insertar la pregunta!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/listar";
	}
	
	@GetMapping("/pregunta/eliminar/{idPregunta}")
	public String delete(@PathVariable("idPregunta") String idPregunta, HttpServletRequest request, HttpServletResponse response) {
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		try {
			iPreguntaService.delete(Integer.parseInt(idPregunta));
			flashMap.put("success", "Se eliminó la pregunta correctamente!");
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al eliminar la pregunta!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/listar";
	}
}
