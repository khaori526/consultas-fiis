package com.unfv.consultasfiis.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.SessionFlashMapManager;

import com.unfv.consultasfiis.entity.Usuario;
import com.unfv.consultasfiis.service.IEscuelaService;
import com.unfv.consultasfiis.service.IRolService;
import com.unfv.consultasfiis.service.IUsuarioService;
import com.unfv.consultasfiis.util.NcCrypt;
import com.unfv.consultasfiis.util.PageRender;

@Controller
public class UsuariosController {

	private Logger logger = LoggerFactory.getLogger(UsuariosController.class);
	
	@Autowired
	private IUsuarioService usuarioService;
	
	@Autowired
	private IEscuelaService escuelaService;
	
	@Autowired
	private IRolService rolService;
	
	@GetMapping(value = {"/usuarios/listar", "/usuarios"})
	public String listarUsuarios(@RequestParam(name = "page", defaultValue = "0") int page, Model model,
			Authentication authentication) {

		if(authentication != null) {
			logger.info("Hola usuario autenticado, tu username es: ".concat(((Usuario)authentication.getPrincipal()).getUsername()));
		}
		
		
		Pageable pageRequest = PageRequest.of(page, 10);

		Page<Usuario> listaUsuarios = usuarioService.findAllByOrderByApellidoPaternoAsc(pageRequest);
		listaUsuarios.stream().forEach(x->x.setPassword(NcCrypt.desencriptarPassword(x.getPassword())));
		PageRender<Usuario> pageRender = new PageRender<Usuario>("/usuarios/listar", listaUsuarios);
		model.addAttribute("listaUsuarios", listaUsuarios);
		model.addAttribute("page", pageRender);
		model.addAttribute("listaEscuelas", escuelaService.findAll());
		model.addAttribute("listaRoles", rolService.findAll());
		
		return "usuarios/listar";
	}
	
	@PostMapping("/usuarios")
	public String save(Usuario usuario, HttpServletRequest request, HttpServletResponse response) 
	{
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		try {
			Usuario insert = usuarioService.saveOrUpdate(usuario);	
			if(insert!=null) {
				System.out.println("Se inserto el usuario correctamente");
				flashMap.put("success", "Se insertó el usuario correctamente!");
			}
			else {
				System.out.println("Error al insertar el usuario");
				flashMap.put("error", "Error al insertar el usuario!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al insertar el usuario!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/usuarios/listar";
	}
	
	@PostMapping("/usuarios/editar")
	public String update(Usuario usuario, HttpServletRequest request, HttpServletResponse response) 
	{
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		try {
			Usuario update = usuarioService.saveOrUpdate(usuario);	
			
			if(update!=null) {
				System.out.println("Se modifico el usuario correctamente");
				flashMap.put("success", "Se modificó el usuario correctamente!");
			}
			else {
				System.out.println("Error al modificar el usuario");
				flashMap.put("error", "Error al modificar el usuario!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al modificar el usuario!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/usuarios/listar";
	}
	
	@GetMapping("/usuarios/eliminar/{idUsuario}")
	public String delete(@PathVariable("idUsuario") String idUsuario, HttpServletRequest request, HttpServletResponse response) {
		
		FlashMap flashMap = new FlashMap();
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		
		try {
			usuarioService.delete(Integer.parseInt(idUsuario));
			flashMap.put("success", "Se eliminó el usuario correctamente!");
		} catch (Exception e) {
			logger.error(e.getMessage());
			flashMap.put("error", "Error al eliminar el usuario!");
		}
		
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		return "redirect:/usuarios/listar";
	}
}

