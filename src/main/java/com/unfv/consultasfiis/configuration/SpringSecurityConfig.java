package com.unfv.consultasfiis.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.unfv.consultasfiis.auth.handler.LoginSuccessHandler;


@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private LoginSuccessHandler successHandler;
	
	
	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;
	
	@Autowired
	public void configurerGlobal(AuthenticationManagerBuilder build) throws Exception{
		build.authenticationProvider(customAuthenticationProvider);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
			.antMatchers(HttpMethod.GET, "/css/**", "/js/**", "/images/**", "/login").permitAll()
			.antMatchers(HttpMethod.GET, "/consultas/listar", "/consultas/").hasAnyRole("ROLE_ADMINISTRADOR", "ROLE_ADMINISTRATIVO", "ROLE_ALUMNO")
//			.antMatchers("/uploads/**").hasAnyRole("ROLE_ASSISTANT")
//			.antMatchers("/form/**").hasAnyRole("ROLE_EXHIBITOR")
//			.antMatchers("/eliminar/**").hasAnyRole("ROLE_EXHIBITOR")
//			.antMatchers("/factura/**").hasAnyRole("ROLE_EXHIBITOR")
			.anyRequest().authenticated()
			.and()
			    .formLogin()
			        .successHandler(successHandler)
			        .loginPage("/login")
			    .permitAll()
			.and()
			.logout().permitAll()
			.and()
			.exceptionHandling().accessDeniedPage("/error_403");

	}
}
