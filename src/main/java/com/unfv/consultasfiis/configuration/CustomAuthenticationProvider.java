package com.unfv.consultasfiis.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.unfv.consultasfiis.entity.Usuario;
import com.unfv.consultasfiis.repository.IUsuarioRepository;
import com.unfv.consultasfiis.util.NcCrypt;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{
	
	@Autowired
	private IUsuarioRepository iUsuarioRepository;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
        String password = authentication.getCredentials()
            .toString();
        
        Optional<Usuario> authenticatedUser = iUsuarioRepository.findByUsernameAndPassword(username, NcCrypt.encriptarPassword(password)).stream().findFirst();
        if(!authenticatedUser.isPresent()){
            throw new BadCredentialsException("Las credenciales son incorrectas");
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        if(authenticatedUser.get().getRol()!=null) {
        	authorities.add(new SimpleGrantedAuthority(authenticatedUser.get().getRol().getNombre()));
        }
        
        Authentication auth = new UsernamePasswordAuthenticationToken(authenticatedUser.get(), password, authorities);
        return auth;
        
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
